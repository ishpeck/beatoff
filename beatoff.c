/* The MIT License (MIT)

Copyright (c) 2013 Anthony "Ishpeck" Tedjamulia

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

#define BEAT_SECOND_RATIO 86.4
#define SECONDS_PER_DAY 86400
#define BMTOFFSET 3600
#define DATE_STRING_FORMAT_NOTATION "YYYY-MM-DD hh:mm:ss"

static int showHelp(const char *proggyName) {
    printf("Ishpeck's .beat calculator version 0.2.\n");
    printf("To display time other than current time, ");
    printf("enter the current time in the form of: \n");
    printf("\t %s '%s'\n", proggyName, DATE_STRING_FORMAT_NOTATION);
    printf("And be sure to enclose that all in quotes so ");
    printf("your shell doesn't mess it up.\n\n");
    printf("To read the time from stdin:\n");
    printf("\t %s -\n", proggyName);
    printf("And be sure the date format from stdin is correct ");
    printf("(%s).\n", DATE_STRING_FORMAT_NOTATION);
    printf("Any number of dates can be supplied. You may separate them with ");
    printf("whitespace if you desire. Each conversion will be ");
    printf("output to a single line. Invalid input will result in termination.\n");
    return 0;
}

static double convertSecondsToBeats(time_t givenSeconds) {
    return ((double)givenSeconds)/BEAT_SECOND_RATIO;
}

static time_t offsetBMT(time_t baseTimeStamp) {
    return baseTimeStamp+BMTOFFSET;
}

static time_t secondsSinceMidnight(time_t baseTime) {
    return baseTime%SECONDS_PER_DAY;
}

static time_t secondsSinceMidnightBMT(time_t unixTime) {
    return secondsSinceMidnight(offsetBMT(unixTime));
}

static int mayBeReadableAsDate(const char *dateString) {
    return strlen(dateString)>=strlen(DATE_STRING_FORMAT_NOTATION);
}

static time_t convertFormattedTime(const char *format, const char *input) {
    struct tm givenTime;
    if(strptime(input, format, &givenTime))
        return mktime(&givenTime);
    fprintf(stderr, "Could not use time string of: %s\n", input);
    exit(1);
}

static time_t readTimeInDefaultFormat(const char *input) {
    return convertFormattedTime("%Y-%m-%d %H:%M:%S", input);
}

static double convertUnixTimeToBeats(time_t secsSinceEpoch) {
    return convertSecondsToBeats(secondsSinceMidnightBMT(secsSinceEpoch));
}

static int showGivenTimeInBeats(time_t secsSinceEpoch) {
    printf("@%f .beats\n", convertUnixTimeToBeats(secsSinceEpoch));
    return 0;
}

static int showCurrentBeats(void) {
    return showGivenTimeInBeats(time(NULL));
}

static int showFormattedTimeInBeats(const char *formattedTime) {
    return showGivenTimeInBeats(readTimeInDefaultFormat(formattedTime));
}

static char consumeWhitespace(void) {
    char c;
    while((c = fgetc(stdin)) == '\n' || c == '\r' || c == ' ' || c == '\t');
    return c;
}

static int showTimeFromStdin(void) {
    int len = strlen(DATE_STRING_FORMAT_NOTATION)+1;
    char in[len];
    while((in[0] = consumeWhitespace()) != EOF) {
        in[1] = '\0';
        fgets(in+1, len-1, stdin);
        showFormattedTimeInBeats(in);
    }
    return 0;
}

int main(int argc, char **argv) {
    for(--argc;argc>0;--argc)
        if(strcmp(argv[argc], "-h")==0) 
            return showHelp(argv[0]);
        else if(mayBeReadableAsDate(argv[argc]))
            return showFormattedTimeInBeats(argv[argc]);
        else if(strcmp(argv[argc], "-")==0)
            return showTimeFromStdin();
    return showCurrentBeats();
}
