all: beatoff
	./beatoff -h
	./beatoff
	./beatoff '2013-06-07 00:00:00'
	./beatoff '2013-06-07 05:00:00'
	./beatoff 'aensutidaeutsgdksgtaeudsgirtaedusitgd' || printf "Good!"
	printf "2013-06-07 00:00:00" | ./beatoff -
	printf "2013-06-07 00:00:00asdfasdfaewwaefatbsrtbrsjrtyju" | ./beatoff - || echo "Good!"
	printf "aensutidaeutsgdksgtaeudsgirtaedusitgd" | ./beatoff - || echo "Good!"
	printf "" | ./beatoff -
	printf "2013-06-07 00:00:002013-06-07 05:00:002016-06-30 03:00:00" | ./beatoff -
	printf "2013-06-07 00:00:002013-06-07 05:00:00asdfasdf" | ./beatoff - || echo "Good!"
	printf "2013-06-07 00:00:00  \r\n    2013-06-07 05:00:00 \n\t  2016-06-30 03:00:00" | ./beatoff -
	printf "\n\n2013-06-07 00:00:00\n\n\n" | ./beatoff -
	printf "\n\n2013-06-07 00:00:00\n\n\na" | ./beatoff - || echo "If you don't see this, then there is a bug!"

beatoff: beatoff.c
	gcc -o beatoff beatoff.c

install: beatoff
	cp beatoff /usr/local/bin/
	chmod 777 /usr/local/bin/beatoff

