# beatoff: Get current time in Swatch .beats

This program puts the current Swatch Internet Time to stdout.

For more information, visit:

[https://en.wikipedia.org/wiki/Swatch_Internet_Time](https://en.wikipedia.org/wiki/Swatch_Internet_Time)

It's better time. 

# Building and Installing

You should be able to build and install like so...

`make && sudo make install`

Unless you're on some derpy proprietary system.

# Use

To get current time in .beats:

`$ beatoff`

To get a specific time in .beats:

`$ beatoff '2009-01-22 23:14:00'`

To read specific, optionally whitespace separated, time(s) from stdin:

`$ beatoff -`

The date parsing is done by POSIX strptime using 
the following format string:

`%Y-%m-%d %H:%M:%S`

# TODO

 1. Open the can of worms that is allowing different timezones.
 
# Note

The common ways of representing time are broken.
